

export const state = [

    {
        "id": "1",
        "name": "User1",
        "surname":"surnameUser1",
        "age": 20,
        "city": "Moscow",
        "street": "baker street",
        "house": 101,
        "email": "google@mail.ru",
        "education": "MLSU"
        
    },
    {
        "id": "2",
        "name": "User2",
        "surname": "surnameUser2",
        "age": 25,
        "city": "NY",
        "street": "zblBitskay street",
        "house": 102,
        "email": "tutby@mail.ru",
        "education": "BSTU"
     
    },
    {
        "id": "3",
        "name": "User3",
        "surname": "surnameUser3",
        "age": 26,
        "city": "Tokio",
        "street": "Oktyaborskay street",
        "house": 103,
        "email": "list@mail.ru",
        "education": "BSTU"
    }
  
]

