import React, {useState} from "react"
import style from "./Styles/PostForma.module.css"


function SendForm(props) {

    const [valueSelect, setValueSelect] = useState('')
    const [valueHeading, setValueHeading] = useState('')
    const [valuePostText, setValuePostText] = useState('')
    const selectOptions = props.state.map((item) => {
        return (<option>{item.name + ' ' + item.surname}</option>)
    })
    const handleChangeSelect = e => {
        setValueSelect(e.target.value)
    }
    const handleChangeHeading = e => {
        setValueHeading(e.target.value)
    }
    const handleChangePostText = e => {
        setValuePostText(e.target.value)
    }
    const handleSubmit = e => {
        e.preventDefault()
        props.onSubmit({
            id: Math.floor(Math.random() * 1000),
            author: valueSelect,
            heading: valueHeading,
            text: valuePostText,
            postDate: new Date().toLocaleTimeString()
        })
        setValueSelect('')
        setValueHeading('')
        setValuePostText('')
    }

    return (
        <form onSubmit={handleSubmit}>
            <h2>MY NEWS.</h2>
            <select onChange={handleChangeSelect} name="select" id="select" value={valueSelect}>
                <option>Change autor</option>
                {selectOptions}
            </select>
            <input value={valueHeading} onChange={handleChangeHeading} type="text" className={style.headingPost}
                   name="headingPost" placeholder="Enter your title..."/>
            <textarea value={valuePostText} onChange={handleChangePostText} className={style.postText} name="postText"
                      placeholder="Your news..." rows="5"/>
            <button type="submit" className={style.sendPost}>Submit</button>
        </form>
    )
}

export default SendForm